<?php

require_once 'HttpStatus.php';

class ServiceResponse {

    public function __construct($status = HttpStatus::BadRequest)
    {
        $this->status = $status;
    }

    public function send($cuerpo){

        if(is_array($cuerpo)){
            $getStatus = $cuerpo['status'];
            if(!empty($getStatus) && is_int($getStatus)){
               $this->status = $getStatus;
            }
        }

       
        http_response_code($this->status);
        header('Content-Type: application/json; charset=utf8');
        header('Access-Control-Allow-Origin: *');
       // header('Access-Control-Allow-Origin: http://127.0.0.1:56111');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, X-Requested-With");


        echo json_encode($cuerpo, JSON_PRETTY_PRINT);
        exit;
    }

    public function sendData($status, $cuerpo){
        http_response_code($status);
        header('Content-Type: application/json; charset=utf8');
        header('Access-Control-Allow-Origin: *');

        echo json_encode($cuerpo, JSON_PRETTY_PRINT);
        exit;
    }

}