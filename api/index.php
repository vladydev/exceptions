<?php
require 'Traces.php';
require 'lib/ServiceResponse.php';

// Create exceptions
$serviceResponse = new ServiceResponse();

$url_req = trim($_SERVER['REQUEST_URI']);

// Extraer segmento de la url
if (isset($url_req)){
    $peticion = explode('/', $url_req)[3];
}
else{
    $messsage = "HttpStatus::NotFound ".utf8_encode($url_req);
    throw new Exception($messsage);
}

switch($peticion){
    case 'delete': {
        $serviceResponse ->send(Traces::delete());
    }
    break;
    case 'getCrashes': {
        $serviceResponse ->send(Traces::getCrashes());
    }
    case 'logger': {
        $serviceResponse ->send(Traces::upsert2());
    }
    case 'telemetry':{
        $serviceResponse ->send(Traces::getUrls());
    }
    default : {
        throw new Exception("Api not define to ".$peticion);
    }
    break;
}
