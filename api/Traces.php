<?php
require 'lib/ConexionBD.php';
require 'lib/HttpStatus.php';
require 'lib/ServiceEmail.php';

class Traces
{

    public static function delete()
    {

        $res = file_get_contents('php://input');

        $jsonData = json_decode($res);

        $type = $jsonData->type;
        $package = $jsonData->id_package;

        if ($type == null || $package == null)
        {
            throw new Exception("Los campos son obligatorios");
        }

        try
        {
            $pdo = ConexionBD::getInstance()->getPDO();

            // construct the delete statement
            $sql = 'DELETE FROM BrowserUrl WHERE url = :type_value And id_package = :id_package_value';

            // prepare the statement for execution
            $statement = $pdo->prepare($sql);
            $statement->bindParam(':type_value', $type, PDO::PARAM_STR);
            $statement->bindParam(':id_package_value', $package, PDO::PARAM_STR);

            // execute the statement
            if ($statement->execute() && $statement->rowCount() > 0)
            {
                return ["status" => HttpStatus::OK, "type" => $type, "id_package" => $package, "total" => $statement->rowCount() ];
            }
            else
            {
                return ["status" => HttpStatus::NotFound, "type" => $type, "id_package" => $package, "sql" => $statement];
            }

        }
        catch(Exception $e)
        {
            throw new Exception("Se ha prodcion un error" . $e);
        }
    }

    public static function getCrashes()
    {
        try
        {
            $pdo = ConexionBD::getInstance()->getPDO();

            $sql = "SELECT * FROM BrowserUrl Order by last_insert DESC";

            $return_arr = array();

            foreach ($pdo->query($sql) as $row)
            {
                $row_array['id'] = $row['id'];
                $row_array['name'] = $row['host'];
                $row_array['url'] = $row['url'];
                $row_array['n_times'] = $row['n_times'];
                $row_array['stack'] = $row['trace'];
                $row_array['device'] = $row['device'];
                $row_array['first_insert'] = $row['first_insert'];
                $row_array['last_insert'] = $row['last_insert'];
                $row_array['id_package'] = $row['id_package'];
                $row_array['version'] = "old";
                array_push($return_arr, $row_array);
            }

            $sql = "SELECT * FROM LogException Order by last_insert DESC";

            foreach ($pdo->query($sql) as $row)
            {
                $row_array['id'] = $row['id'];
                $row_array['name'] = $row['title'];
                $row_array['url'] = $row['message'];
                $row_array['n_times'] = $row['n_times'];
                $row_array['stack'] = urldecode($row['stack']);
                $row_array['device'] = $row['device'];
                $row_array['first_insert'] = $row['first_insert'];
                $row_array['last_insert'] = $row['last_insert'];
                $row_array['id_package'] = $row['id_package'];
                $row_array['version'] = $row['version'];
                array_push($return_arr, $row_array);
            }

            return ["status" => HttpStatus::OK, "crashes" => $return_arr];

        }
        catch(Exception $e)
        {
            throw new Exception("Los campos son obligatorios" . $e);
        }
    }

    public static function getUrls(){
        try
        {
            $pdo = ConexionBD::getInstance()->getPDO();

            $sql = "SELECT * FROM WebPage Order by lastInsert DESC";

            $return_arr = array();

            foreach ($pdo->query($sql) as $row)
            {
                $row_array['id'] = $row['_id'];
                $row_array['host'] = $row['mHost'];
                $row_array['url'] = $row['mUrl'];
                $row_array['nTimes'] = $row['nTimes'];
                
                array_push($return_arr, $row_array);
            }
            return ["status" => HttpStatus::OK, "urls" => $return_arr];

        }
        catch(Exception $e)
        {
            throw new Exception("Los campos son obligatorios" . $e);
        }
    }

    public static function upsert()
    {

        try
        {

            $cuerpo = file_get_contents('php://input');
            $json = json_decode($cuerpo);
            $type = $json->type;
            $stack = $json->stack;

            $device = 'Not define';
            $id_package = 'com.altamirano.fabricio.tvbrowser';

            if (str_contains($cuerpo, '"device":'))
            {
                $device = $json->device;
                $device = urldecode($device);
            }

            if (str_contains($cuerpo, '"id_package":'))
            {
                $id_package = $json->id_package;
            }

            $trace = urldecode($stack);

            //Catch error https://www.easysite.one/Play/ita/Ch5.php
            $url = str_replace("Catch error", "", $type);
            $url = ltrim($url);
            $host = parse_url($url, PHP_URL_HOST); //hostname
            if ($host == null)
            {
                $host = $type;
            }

            $pdo = ConexionBD::getInstance()->getPDO();

            $sql = "SELECT id, n_times FROM BrowserUrl where url ='" . $url . "' AND id_package ='" . $id_package . "'";
            $id = 0;
            $n_times = 0;
            foreach ($pdo->query($sql) as $row)
            {
                $id = $row['id'];
                $n_times = $row['n_times'];
            }

            $n_times++;

            if ($id != 0)
            {

                $date = date("Y-m-d H:i:s");
                //Aqui colocas el código que tu deseas realizar cuando el dato existe en la base de datos
                $comando = "UPDATE  BrowserUrl SET n_times='" . $n_times . "', last_insert='" . $date . "' WHERE id ='" . $id . "'";
                $sentencia = $pdo->prepare($comando);
                $resultado = $sentencia->execute();

            }
            else
            {
                //Aqui colocas el código que tu deseas realizar cuando el dato NO existe en la base de datos
                // Sentencia INSERT
                $comando = "INSERT INTO BrowserUrl (host,url,n_times,trace,device, id_package) VALUES(?,?,?,?,?,?)";

                $sentencia = $pdo->prepare($comando);

                $sentencia->bindParam(1, $host);
                $sentencia->bindParam(2, $url);
                $sentencia->bindParam(3, $n_times);
                $sentencia->bindParam(4, $trace);
                $sentencia->bindParam(5, $device);
                $sentencia->bindParam(6, $id_package);
                $resultado = $sentencia->execute();

            }

            if (!str_contains($type, 'Catch error'))
            {
                ServiceEmail::sendEmail($type, $trace, $device);
            }

            return ["status" => HttpStatus::OK, "crash" => "Log saved"];

        }
        catch(Exception $e)
        {
            echo $e;
        }
    }

    public static function upsert2()
    {

        try
        {
            // Traces::normalize();
            $cuerpo = file_get_contents('php://input');
            $json = json_decode($cuerpo);
            $message = $json->type;
            $stack = $json->stack;

            $device = 'Not define';
            $id_package = 'com.altamirano.fabricio.tvbrowser';
            $version = '4.0.0';

            if (str_contains($cuerpo, '"device":'))
            {
                $device = $json->device;
                $device = urldecode($device);
            }

            if (str_contains($cuerpo, '"id_package":'))
            {
                $id_package = $json->id_package;
            }

            if (str_contains($cuerpo, '"version":'))
            {
                $version = $json->version;
            }
            else
            {
                $skuList = explode(PHP_EOL, $device);
                $version = substr($skuList[1], 14);
            }

            $stack = urldecode($stack);

            $title = str_replace("Catch error", "", $message);
            $title = ltrim($title);
            try
            {
                $skuList = explode(PHP_EOL, $stack);
                $title = $skuList[1];
            }
            catch(Exception $e)
            {
                $title = $message;
            }


            if (str_contains($cuerpo, "Catch error http"))
            {
               
                //Catch error https://www.easysite.one/Play/ita/Ch5.php
                $url = str_replace("Catch error", "", $message);
                $url = ltrim($url);
                $host = parse_url($url, PHP_URL_HOST); //hostname
                if ($host == null)
                {
                    $host = $url;
                }

                Traces::logUrl($host, $url);
                return ["status" => HttpStatus::OK, "crash" => "Log Url saved"];
            }



            $title = str_replace("at " . $id_package . ".", "", $title);

            $pdo = ConexionBD::getInstance()->getPDO();

            $sql = "SELECT id, n_times FROM LogException where message ='" . $message . "' AND id_package ='" . $id_package . "'";
            $id = 0;
            $n_times = 0;
            foreach ($pdo->query($sql) as $row)
            {
                $id = $row['id'];
                $n_times = $row['n_times'];
            }

            $n_times++;

            if ($id != 0)
            {

                $date = date("Y-m-d H:i:s");
                //Aqui colocas el código que tu deseas realizar cuando el dato existe en la base de datos
                $comando = "UPDATE  LogException SET n_times='" . $n_times . "', last_insert='" . $date . "' WHERE id ='" . $id . "'";
                $sentencia = $pdo->prepare($comando);
                $resultado = $sentencia->execute();

            }
            else
            {
                //Aqui colocas el código que tu deseas realizar cuando el dato NO existe en la base de datos
                // Sentencia INSERT
                $comando = "INSERT INTO LogException (id_package,version,title,message,n_times, stack, device) VALUES(?,?,?,?,?,?,?)";

                $sentencia = $pdo->prepare($comando);

                $sentencia->bindParam(1, $id_package);
                $sentencia->bindParam(2, $version);
                $sentencia->bindParam(3, $title);

                $sentencia->bindParam(4, $message);
                $sentencia->bindParam(5, $n_times);
                $sentencia->bindParam(6, $stack);
                $sentencia->bindParam(7, $device);

                $resultado = $sentencia->execute();

            }

            if (!str_contains($message, 'Catch error'))
            {
                ServiceEmail::sendEmail($message, $stack, $device);
            }

            return ["status" => HttpStatus::OK, "crash" => "Log saved v2"];

        }
        catch(Exception $e)
        {
            echo $e;
        }
    }

    public static function normalize()
    {

        $pdo = ConexionBD::getInstance()->getPDO();

        $sql = "SELECT id, title, id_package FROM LogException";
        $id = 0;
        $title = "";

        foreach ($pdo->query($sql) as $row)
        {
            $id = $row['id'];
            $title = $row['title'];
            $id_package = $row['id_package'];

            if ($id != 0)
            {

                $title = str_replace("at " . $id_package . ".", "", $title);
                //Aqui colocas el código que tu deseas realizar cuando el dato existe en la base de datos
                $comando = "UPDATE  LogException SET title='" . $title . "' WHERE id ='" . $id . "'";
                $sentencia = $pdo->prepare($comando);
                $resultado = $sentencia->execute();
            }

        }

    }

    public static function logUrl($host, $url){

        try
        {
            $pdo = ConexionBD::getInstance()->getPDO();

            $sql = "SELECT _id, nTimes FROM WebPage where mUrl ='".$url ."'";
            $id = 0;
            $n_times = 0;
            foreach ($pdo->query($sql) as $row)
            {
                $id = $row['_id'];
                $n_times = $row['nTimes'];
            }

            $n_times++;

            if ($id != 0)
            {

                $date = date("Y-m-d H:i:s");
               
                $comando = "UPDATE  WebPage SET nTimes='" . $n_times . "', lastInsert='" . $date . "' WHERE _id ='" . $id . "'";
                $sentencia = $pdo->prepare($comando);
                $resultado = $sentencia->execute();

            }
            else
            {
                $comando = "INSERT INTO WebPage (mHost,mUrl,nTimes) VALUES(?,?,?)";

                $sentencia = $pdo->prepare($comando);

                $sentencia->bindParam(1, $host);
                $sentencia->bindParam(2, $url);
                $sentencia->bindParam(3, $n_times);
                $resultado = $sentencia->execute();

            }

        }catch(Exception $e){
            echo $e;
        }

        
    }

}